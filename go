#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

set -eu

readonly RED='\033[91m'
readonly RESET='\033[0m'
readonly WHITE='\033[97m'

# shellcheck disable=SC2155
# The output from these command expansions cannot reasonably fail.
readonly script_name="$(basename "$0")"
# shellcheck disable=SC2155
readonly script_path="$(dirname "$0")"

function main() {
  local -r command="${1:-help}"

  case "${command}" in
  init)
    require poetry
    require pre-commit
    install_hooks
    install_project
    ;;
  help)
    show_usage
    ;;
  lint)
    require poetry
    require shellcheck
    require shfmt
    install_dependencies
    run_linter
    ;;
  shell)
    require poetry
    install_project
    run_shell
    ;;
  test)
    require poetry
    install_dependencies
    run_tests
    ;;
  *)
    invalid "${command}"
    ;;
  esac
}

function show_usage() {
  echo -e "$(
    cat <<EOF
Usage: ${script_path}/${script_name} COMMAND...

Commands:
  ${WHITE}help${RESET}    Shows this help text.
  ${WHITE}init${RESET}    Installs commit hooks and sets up the virtual environment.
  ${WHITE}lint${RESET}    Lints the source code.
  ${WHITE}shell${RESET}   Enter a shell using the virtual environment.
  ${WHITE}test${RESET}    Runs the test suite.
EOF
  )" 1>&2
}

function run_linter() {
  echo -e "\n${WHITE}Linting...${RESET}\n" 1>&2
  poetry run pylint git_template
  # shellcheck disable=SC2046
  # A list of files is expected so the output from find should be expanded as
  # separate words.
  shellcheck "${script_name}" $(find . -name '*.sh')
  shfmt --indent=2 --write .
}

function run_shell() {
  echo -e "\n${WHITE}Starting a shell... Type 'exit' to quit.${RESET}\n" 1>&2
  poetry shell
}

function run_tests() {
  echo -e "\n${WHITE}Running tests...${RESET}\n" 1>&2
  poetry run pytest
}

function invalid() {
  local -r command="$1"
  echo -e "${RED}Error: unknown command '${command}'.\n${RESET}" 1>&2
  show_usage
  return 255
}

function require() {
  local -r program="$1"
  if ! command -v "${program}" >/dev/null; then
    echo -e "${RED}Error: '${program}' is not installed!${RESET}" 1>&2
    return 254
  fi
}

function install_dependencies() {
  poetry install --no-root
}

function install_hooks() {
  echo -e "${WHITE}Installing commit hooks...${RESET}\n" 1>&2
  pre-commit install
  echo 1>&2
}

function install_project() {
  echo -e "${WHITE}Installing project in virtual environment...${RESET}\n" 1>&2
  poetry install
}

pushd "${script_path}" >/dev/null
main "$@"
popd >/dev/null
