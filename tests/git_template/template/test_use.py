# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from pathlib import Path

import pytest
from hamcrest import assert_that, equal_to, is_

from git_template.template.context import ContextValues
from git_template.template.repository import Repository
from git_template.template.use import use_layout
from ..matcher import has_exited_successfully


@pytest.fixture()
def example_layout(layouts_directory) -> None:
    with layouts_directory.joinpath("example.liquid").open("w") as template:
        template.write("content")


@pytest.fixture()
def context(layout_store, git_repo) -> ContextValues:
    return ContextValues(
        layouts=layout_store,
        repo=Repository(git_repo.path),
    )


class TestUse:
    def test_writes_template_to_git_repository(
        self, runner, context, example_layout, git_repo, monkeypatch
    ) -> None:
        monkeypatch.chdir(git_repo.path)

        # noinspection PyTypeChecker
        result = runner.invoke(use_layout, ["example"], obj=context)

        assert_that(result, has_exited_successfully())
        template = (Path(git_repo.controldir()) / "templates" / "example").read_text()
        assert_that(template, is_(equal_to("content")))

    def test_writes_template_location_to_git_repository_config(
        self, runner, context, example_layout, git_repo, monkeypatch
    ) -> None:
        git_repo_path = Path(git_repo.path)
        relative_control_path = Path(git_repo.controldir()).relative_to(git_repo_path)
        expected_template_path = str(relative_control_path / "templates" / "example")
        monkeypatch.chdir(git_repo_path)

        # noinspection PyTypeChecker
        runner.invoke(use_layout, ["example"], obj=context)

        config = git_repo.get_config()
        commit_template_path = str(config.get("commit", "template"), encoding="utf-8")
        assert_that(commit_template_path, is_(equal_to(expected_template_path)))

    def test_writes_last_used_layout_to_git_repository_config(
        self, runner, context, example_layout, git_repo, monkeypatch
    ) -> None:
        monkeypatch.chdir(git_repo.path)

        # noinspection PyTypeChecker
        runner.invoke(use_layout, ["example"], obj=context)

        config = git_repo.get_config()
        layout_name = str(
            config.get(("commit", "template"), "layout"), encoding="utf-8"
        )
        assert_that(layout_name, is_(equal_to("example")))
