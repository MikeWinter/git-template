# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from pathlib import Path
from unittest.mock import patch

from hamcrest import assert_that, contains_exactly, empty, equal_to, is_, is_not

from git_template.template.entrypoint import run
from ..matcher import has_exited_successfully


class TestConsole:
    def test_can_use_layout(
        self, runner, git_repo, layouts_directory, monkeypatch
    ) -> None:
        monkeypatch.chdir(git_repo.path)
        with layouts_directory.joinpath("example.liquid").open("w") as layout:
            layout.write("test content")

        # noinspection PyTypeChecker
        result = runner.invoke(run, ["use", "example"])

        assert_that(result, has_exited_successfully())
        template_filename = str(
            git_repo.get_config().get("commit", "template"), encoding="utf-8"
        )
        # noinspection PyTypeChecker
        assert_that(template_filename, is_not(empty()))
        with open(template_filename, "r") as rendered_template:
            assert_that(rendered_template.read(), is_(equal_to("test content")))

    @patch("git_template.template.entrypoint.LayoutStore")
    def test_can_use_layouts_in_other_data_directories(
        self, mock_layout_store, runner, git_repo, monkeypatch
    ) -> None:
        monkeypatch.chdir(git_repo.path)
        monkeypatch.setenv("XDG_DATA_HOME", "/first")
        monkeypatch.setenv("XDG_DATA_DIRS", "/second:/third")

        # noinspection PyTypeChecker
        runner.invoke(run, ["use", "example"])

        search_paths = mock_layout_store.call_args[0][0]
        assert_that(
            search_paths,
            contains_exactly(
                Path("/first/git-template/layouts"),
                Path("/second/git-template/layouts"),
                Path("/third/git-template/layouts"),
            ),
        )
