# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from hamcrest import assert_that, calling, equal_to, has_property, is_, raises

from git_template.template.layout import LayoutNotFoundError, LayoutStore


class TestLayoutStore:
    def test_loads_layouts_from_search_paths(self, layouts_directory) -> None:
        store = LayoutStore([layouts_directory])
        example_layout = layouts_directory / "example.liquid"
        example_layout.touch()

        layout = store.by_name("example")

        # noinspection PyTypeChecker
        assert_that(layout, has_property("path", equal_to(example_layout)))

    def test_raises_error_if_layout_cannot_be_found(self, layouts_directory) -> None:
        store = LayoutStore([layouts_directory])

        assert_that(
            calling(store.by_name).with_args("does-not-exist"),
            raises(LayoutNotFoundError),
        )

    def test_loads_the_first_matching_layout(
        self, layouts_directory, extra_layouts_directory
    ) -> None:
        store = LayoutStore([layouts_directory, extra_layouts_directory])
        first_layout = layouts_directory / "example.liquid"
        first_layout.touch()
        second_layout = extra_layouts_directory / "example.liquid"
        second_layout.touch()

        layout = store.by_name("example")

        # noinspection PyTypeChecker
        assert_that(layout, has_property("path", equal_to(first_layout)))

    def test_searches_all_paths_for_a_match(
        self, layouts_directory, extra_layouts_directory
    ) -> None:
        store = LayoutStore([layouts_directory, extra_layouts_directory])
        example_layout = extra_layouts_directory / "lower-priority.liquid"
        example_layout.touch()

        layout = store.by_name("lower-priority")

        # noinspection PyTypeChecker
        assert_that(layout, has_property("path", equal_to(example_layout)))


class TestLayout:
    def test_renders_the_template(self, layouts_directory, tmp_path) -> None:
        layout_path = layouts_directory / "test.liquid"
        with layout_path.open("w", encoding="utf-8") as layout_file:
            layout_file.write("some example content")
        store = LayoutStore([layouts_directory])
        template_path = tmp_path / "template.txt"

        layout = store.by_name("test")
        with template_path.open("w", encoding="utf-8") as template_file:
            layout.render(template_file)

        assert_that(
            template_path.read_text(encoding="utf-8"),
            is_(equal_to("some example content")),
        )
