# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from pathlib import Path
from typing import Generator

import pytest
from click.testing import CliRunner
from dulwich.repo import Repo

from git_template.template.layout import LayoutStore


def make_layout_path_structure(path: Path) -> Path:
    layouts = path.joinpath("git-template", "layouts")
    layouts.mkdir(parents=True, exist_ok=True)
    return layouts


@pytest.fixture()
def runner() -> CliRunner:
    return CliRunner(mix_stderr=False)


@pytest.fixture()
def home_directory(monkeypatch, tmp_path) -> Generator[Path, None, None]:
    home = tmp_path / "home"
    home.mkdir()
    monkeypatch.setenv("HOME", str(home))
    yield home


@pytest.fixture()
def home_data_directory(monkeypatch, home_directory) -> Generator[Path, None, None]:
    data = home_directory / "data"
    data.mkdir()
    monkeypatch.setenv("XDG_DATA_HOME", str(data))
    yield data


@pytest.fixture()
def extra_data_directory(monkeypatch, tmp_path) -> Generator[Path, None, None]:
    extra = tmp_path / "extra-data"
    extra.mkdir()
    monkeypatch.setenv("XDG_DATA_DIRS", str(extra))
    yield extra


@pytest.fixture()
def layouts_directory(home_data_directory) -> Path:
    return make_layout_path_structure(home_data_directory)


@pytest.fixture()
def extra_layouts_directory(extra_data_directory) -> Path:
    return make_layout_path_structure(extra_data_directory)


@pytest.fixture()
def git_repo(tmp_path) -> Generator[Repo, None, None]:
    path = tmp_path / "repo"
    path.mkdir()
    with Repo.init(str(path)) as repo:
        yield repo


@pytest.fixture()
def layout_store(layouts_directory, extra_layouts_directory) -> LayoutStore:
    return LayoutStore([layouts_directory, extra_layouts_directory])
