# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

import pytest
from click import BadParameter, Command, Context
from hamcrest import (
    assert_that,
    calling,
    equal_to,
    has_property,
    instance_of,
    is_,
    raises,
)
from pytest_mock import MockerFixture

from git_template.template.context import ContextValues
from git_template.template.layout import Layout, LayoutStore
from git_template.template.params import LAYOUT, LayoutParamType


@pytest.fixture()
def layout_context(mocker: MockerFixture, layouts_directory) -> Context:
    repo_mock = mocker.Mock()
    command_mock = mocker.Mock(Command)
    context_mock = mocker.Mock(Context)
    context_mock.command = command_mock
    context_mock.find_object.return_value = ContextValues(
        layouts=LayoutStore([layouts_directory]),
        repo=repo_mock,
    )
    return context_mock


class TestLayoutParamType:
    def test_type_has_a_name(self) -> None:
        assert_that(LayoutParamType.name, is_(equal_to("layout")))

    def test_conversion_returns_matching_layout_in_search_path(
        self, layout_context, layouts_directory
    ) -> None:
        example_layout = layouts_directory / "example.liquid"
        example_layout.touch()

        value = LAYOUT.convert("example", None, layout_context)

        # noinspection PyTypeChecker
        assert_that(value, is_(instance_of(Layout)))
        # noinspection PyTypeChecker
        assert_that(value, has_property("path", equal_to(example_layout)))

    def test_conversion_raises_error_if_layout_cannot_be_found(
        self, layout_context
    ) -> None:
        assert_that(
            calling(LAYOUT.convert).with_args("does-not-exist", None, layout_context),
            raises(
                BadParameter, pattern="unable to find a layout named 'does-not-exist'"
            ),
        )
