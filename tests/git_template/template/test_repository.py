# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from pathlib import Path

import pytest
from hamcrest import (
    assert_that,
    calling,
    equal_to,
    has_key,
    has_length,
    instance_of,
    is_,
    not_,
    raises,
)

from git_template.template.repository import (
    Configuration,
    NoGitRepositoryFoundError,
    Repository,
)


class TestRepository:
    @pytest.mark.parametrize("start_path", [".", "./subdirectory"])
    def test_finds_the_working_tree(
        self, git_repo, monkeypatch, start_path: str
    ) -> None:
        monkeypatch.chdir(git_repo.path)
        expected_path = Path(git_repo.path)
        Path(git_repo.path).joinpath("subdirectory").mkdir()

        repo = Repository.discover(start_path)

        assert_that(repo.work_tree_path, is_(equal_to(expected_path)))

    def test_raises_an_error_if_outside_of_a_repository(self, tmp_path) -> None:
        with pytest.raises(NoGitRepositoryFoundError) as exc_info:
            Repository(tmp_path)

        assert_that(exc_info.value.path, is_(equal_to(tmp_path)))

    def test_finds_the_common_directory(self, git_repo, monkeypatch) -> None:
        monkeypatch.chdir(git_repo.path)
        expected_path = Path(f"{git_repo.path}/.git")

        repo = Repository.discover()

        assert_that(repo.common_path, is_(equal_to(expected_path)))

    def test_finds_the_git_directory(self, git_repo, monkeypatch) -> None:
        monkeypatch.chdir(git_repo.path)
        expected_path = Path(f"{git_repo.path}/.git")

        repo = Repository.discover()

        assert_that(repo.git_path, is_(equal_to(expected_path)))

    def test_provides_repository_configuration(self, git_repo) -> None:
        repo = Repository(git_repo.path)

        # noinspection PyTypeChecker
        assert_that(repo.configuration, is_(instance_of(Configuration)))


class TestConfiguration:
    @pytest.mark.parametrize(
        ["section", "raw_section"],
        [
            ["section", (b"section",)],
            [("test",), (b"test",)],
            ["name.section", (b"name", b"section")],
            [["section", "subsection"], (b"section", b"subsection")],
            [
                ("section", "subsection", "sub-subsection"),
                (b"section", b"subsection.sub-subsection"),
            ],
            ["foo.bar.baz", (b"foo", b"bar.baz")],
        ],
    )
    def test_can_read_values(self, git_repo, section, raw_section) -> None:
        config = git_repo.get_config()
        config.set(raw_section, "the-key", "the-value")
        config.write_to_path()
        repo = Repository(git_repo.path)

        value = repo.configuration.get(section, "the-key")

        assert_that(value, is_(equal_to(b"the-value")))

    @pytest.mark.parametrize(
        ["section", "raw_section"],
        [
            ["section", (b"section",)],
            [("test",), (b"test",)],
            ["name.section", (b"name", b"section")],
            [["section", "subsection"], (b"section", b"subsection")],
            [
                ("section", "subsection", "sub-subsection"),
                (b"section", b"subsection.sub-subsection"),
            ],
            ["foo.bar.baz", (b"foo", b"bar.baz")],
        ],
    )
    def test_can_update_values(self, git_repo, section, raw_section) -> None:
        repo = Repository(git_repo.path)

        repo.configuration.set(section, "another-key", b"a-different-value")

        config = git_repo.get_config()
        value = config.get(raw_section, "another-key")
        assert_that(value, is_(equal_to(b"a-different-value")))

    @pytest.mark.parametrize(
        ["section", "raw_section"],
        [
            ["section", (b"section",)],
            [("test",), (b"test",)],
            ["name.section", (b"name", b"section")],
            [["section", "subsection"], (b"section", b"subsection")],
            [
                ("section", "subsection", "sub-subsection"),
                (b"section", b"subsection.sub-subsection"),
            ],
            ["foo.bar.baz", (b"foo", b"bar.baz")],
        ],
    )
    def test_can_remove_key(self, git_repo, section, raw_section) -> None:
        config = git_repo.get_config()
        config.set(raw_section, "a-key", b"some-value")
        config.write_to_path()
        repo = Repository(git_repo.path)

        repo.configuration.remove(section, "a-key")

        config = git_repo.get_config()
        assert_that(config[raw_section], not_(has_key(b"a-key")))

    def test_removing_a_nonexistent_key_does_nothing(self, git_repo) -> None:
        repo = Repository(git_repo.path)

        assert_that(
            calling(repo.configuration.remove).with_args("section", "key"),
            not_(raises(Exception)),
        )

    def test_setting_the_same_key_overwrites_the_existing_value(self, git_repo) -> None:
        repo = Repository(git_repo.path)
        configuration = repo.configuration

        configuration.set("section", "key", b"value")
        configuration.set("section", "key", b"value")

        config = git_repo.get_config()
        items = list(config.items((b"section",)))
        # noinspection PyTypeChecker
        assert_that(items, has_length(1))
