# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from click import BadParameter
from hamcrest import assert_that, calling, equal_to, is_, raises

from git_template.params.name import NameParamType


class TestNameParamType:
    def test_type_has_a_name(self) -> None:
        assert_that(NameParamType.name, is_(equal_to("name")))

    def test_conversion_returns_name_unmodified_if_valid(self) -> None:
        param_type = NameParamType()

        value = param_type.convert("Alice Smith", None, None)

        assert_that(value, is_(equal_to("Alice Smith")))

    def test_raises_exception_if_name_is_invalid(self) -> None:
        param_type = NameParamType()

        assert_that(
            calling(param_type.convert).with_args(
                "name.like.email@example.org", None, None
            ),
            raises(
                BadParameter,
                pattern="'name.like.email@example.org' must not contain an email address",
            ),
        )
