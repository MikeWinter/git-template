# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from click import BadParameter
from hamcrest import assert_that, calling, equal_to, is_, raises

from git_template.params.email import EmailParamType


class TestEmailParamType:
    def test_type_has_a_name(self) -> None:
        assert_that(EmailParamType.name, is_(equal_to("email address")))

    def test_conversion_returns_email_address_unmodified_if_valid(self) -> None:
        param_type = EmailParamType()

        value = param_type.convert("mail@example.org", None, None)

        assert_that(value, is_(equal_to("mail@example.org")))

    def test_raises_exception_if_address_is_invalid(self) -> None:
        param_type = EmailParamType()

        assert_that(
            calling(param_type.convert).with_args("invalid-address", None, None),
            raises(BadParameter, pattern="'invalid-address' is not an email address"),
        )
