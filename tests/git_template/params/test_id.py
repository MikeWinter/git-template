# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from click import BadParameter
from hamcrest import assert_that, calling, equal_to, is_, raises

from git_template.params.id import IdParamType


class TestIdParamType:
    def test_type_has_a_name(self) -> None:
        assert_that(IdParamType.name, is_(equal_to("ID")))

    def test_conversion_returns_id_unmodified_if_valid(self) -> None:
        param_type = IdParamType()

        value = param_type.convert("abc1", None, None)

        assert_that(value, is_(equal_to("abc1")))

    def test_raises_exception_if_id_is_invalid(self) -> None:
        param_type = IdParamType()

        assert_that(
            calling(param_type.convert).with_args("invalid-id", None, None),
            raises(BadParameter, pattern="'invalid-id' is not an ID"),
        )
