# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

import subprocess
from pathlib import Path
from typing import Optional

import pytest
from hamcrest import (
    assert_that,
    contains_exactly,
    contains_inanyorder,
    empty,
    equal_to,
    is_,
    none,
)

from git_template.git.config import Configuration


@pytest.fixture(autouse=True)
def git_repo(tmp_path) -> Path:
    subprocess.run(["git", "init"], cwd=tmp_path)
    return tmp_path


@pytest.fixture()
def configuration(git_repo) -> Configuration:
    return Configuration(git_repo)


@pytest.mark.slow()
class TestConfiguration:
    def test_returns_no_keys_when_configuration_is_empty(self, configuration) -> None:
        keys = configuration.keys("section")

        # noinspection PyTypeChecker
        assert_that(keys, is_(empty()))

    def test_reads_keys_from_configuration_section(
        self, configuration, git_repo
    ) -> None:
        subprocess.run(["git", "config", "section.key1", "value"], cwd=git_repo)
        subprocess.run(["git", "config", "section.key2", "value"], cwd=git_repo)

        keys = configuration.keys("section")

        assert_that(keys, contains_inanyorder("key1", "key2"))

    def test_does_not_read_keys_from_other_sections(
        self, configuration, git_repo
    ) -> None:
        subprocess.run(["git", "config", "section1.key1", "value"], cwd=git_repo)
        subprocess.run(["git", "config", "section1.key1.key2", "value"], cwd=git_repo)
        subprocess.run(["git", "config", "section2.key3", "value"], cwd=git_repo)

        keys = configuration.keys("section1")

        assert_that(keys, contains_exactly("key1"))

    def test_reads_keys_from_subsections(self, configuration, git_repo) -> None:
        subprocess.run(["git", "config", "section.sub.key1", "value"], cwd=git_repo)
        subprocess.run(["git", "config", "section.sub.key2", "value"], cwd=git_repo)

        keys = configuration.keys("section", "sub")

        assert_that(keys, contains_inanyorder("key1", "key2"))

    def test_does_not_read_keys_from_other_subsections(
        self, configuration, git_repo
    ) -> None:
        subprocess.run(["git", "config", "section1.key1", "value"], cwd=git_repo)
        subprocess.run(["git", "config", "section1.key1.key2", "value"], cwd=git_repo)
        subprocess.run(
            ["git", "config", "section1.key1.key2.key3", "value"], cwd=git_repo
        )
        subprocess.run(["git", "config", "section2.key1.key4", "value"], cwd=git_repo)

        keys = configuration.keys("section1", "key1")

        assert_that(keys, contains_exactly("key2"))

    @pytest.mark.parametrize(
        ["fully_qualified_name", "section", "subsection", "key"],
        [
            ["example.name", "example", None, "name"],
            ["example.section.key", "example", "section", "key"],
        ],
    )
    def test_returns_value_with_matching_key(
        self,
        configuration,
        git_repo,
        fully_qualified_name: str,
        section: str,
        subsection: Optional[str],
        key: str,
    ) -> None:
        subprocess.run(
            ["git", "config", fully_qualified_name, "Jane Smith <jane@example.org>"],
            cwd=git_repo,
        )

        value = configuration.get(section=section, subsection=subsection, key=key)

        assert_that(value, is_(equal_to(b"Jane Smith <jane@example.org>")))

    def test_returns_none_if_no_key_matches_and_no_default_is_given(
        self, configuration, git_repo
    ) -> None:
        subprocess.run(
            ["git", "config", "example.name", "Jane Smith <jane@example.org>"],
            cwd=git_repo,
        )

        value = configuration.get("example", "names")

        assert_that(value, is_(none()))

    def test_returns_default_if_no_key_matches(self, configuration, git_repo) -> None:
        value = configuration.get("missing", "key", default=b"default value")

        assert_that(value, is_(b"default value"))

    def test_does_not_return_default_if_value_is_blank(
        self, configuration, git_repo
    ) -> None:
        subprocess.run(["git", "config", "blank.key", ""], cwd=git_repo)

        value = configuration.get("blank", "key", default=b"default value")

        # noinspection PyTypeChecker
        assert_that(value, is_(empty()))

    def test_writes_value_to_configuration_section(
        self, configuration, git_repo
    ) -> None:
        configuration.set("section", "key", b"value")

        result = subprocess.run(
            ["git", "config", "--null", "--get", "section.key"],
            cwd=git_repo,
            stdout=subprocess.PIPE,
            stderr=subprocess.DEVNULL,
        )
        assert_that(result.returncode, is_(equal_to(0)))
        assert_that(result.stdout[:-1], is_(equal_to(b"value")))

    def test_writes_value_to_configuration_subsection(
        self, configuration, git_repo
    ) -> None:
        configuration.set("section", "key", b"value", subsection="sub")

        result = subprocess.run(
            ["git", "config", "--null", "--get", "section.sub.key"],
            cwd=git_repo,
            stdout=subprocess.PIPE,
            stderr=subprocess.DEVNULL,
        )
        assert_that(result.returncode, is_(equal_to(0)))
        assert_that(result.stdout[:-1], is_(equal_to(b"value")))

    @pytest.mark.parametrize(
        ["fully_qualified_name", "section", "subsection", "key"],
        [
            ["example.name", "example", None, "name"],
            ["example.section.key", "example", "section", "key"],
        ],
    )
    def test_writes_empty_values_to_configuration(
        self,
        configuration,
        git_repo,
        fully_qualified_name: str,
        section: str,
        subsection: str,
        key: str,
    ) -> None:
        configuration.set(section=section, subsection=subsection, key=key, value=b"")

        result = subprocess.run(
            ["git", "config", "--null", "--get", fully_qualified_name],
            cwd=git_repo,
            stdout=subprocess.PIPE,
            stderr=subprocess.DEVNULL,
        )
        assert_that(result.returncode, is_(equal_to(0)))
        # noinspection PyTypeChecker
        assert_that(result.stdout[:-1], is_(empty()))

    def test_removes_key_from_configuration(self, configuration, git_repo) -> None:
        subprocess.run(["git", "config", "section.key", "value"], cwd=git_repo)

        configuration.unset(section="section", key="key")

        result = subprocess.run(
            ["git", "config", "--get", "section.key"],
            cwd=git_repo,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
        )
        assert_that(result.returncode, is_(equal_to(1)))

    def test_removes_key_from_configuration_subsection(
        self, configuration, git_repo
    ) -> None:
        subprocess.run(["git", "config", "section.sub.key", "value"], cwd=git_repo)

        configuration.unset(section="section", subsection="sub", key="key")

        result = subprocess.run(
            ["git", "config", "--get", "section.sub.key"],
            cwd=git_repo,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
        )
        assert_that(result.returncode, is_(equal_to(1)))
