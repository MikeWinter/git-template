# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import subprocess
from pathlib import Path

import click.testing
import pytest
from hamcrest import assert_that, equal_to, matches_regexp
from pytest_mock import MockerFixture

from git_template.coauthor.console import run
from ..matcher import (
    has_error,
    has_exited_exceptionally,
    has_exited_successfully,
    has_output,
)


@pytest.fixture(autouse=True)
def git_repo(tmp_path) -> Path:
    subprocess.run(["git", "init"], cwd=tmp_path)
    cwd = os.getcwd()
    os.chdir(tmp_path)
    yield tmp_path
    os.chdir(cwd)


@pytest.fixture()
def runner() -> click.testing.CliRunner:
    return click.testing.CliRunner(mix_stderr=False)


@pytest.fixture()
def configuration(mocker: MockerFixture):
    configuration = mocker.Mock()
    configuration.keys.return_value = ["id"]
    configuration.get.return_value = b"value"

    configuration_cls = mocker.patch("git_template.coauthor.console.Configuration")
    configuration_cls.return_value = configuration


def test_can_invoke_list_command(runner) -> None:
    # noinspection PyTypeChecker
    result = runner.invoke(run, ["list"])

    assert_that(result, has_exited_successfully())


def test_can_retrieve_added_coauthors(runner) -> None:
    # noinspection PyTypeChecker
    add_result = runner.invoke(
        run,
        ["add", "--id", "js", "--name", "Jane Smith", "--email", "jane@example.org"],
    )
    assert_that(add_result, has_exited_successfully())

    # noinspection PyTypeChecker
    list_result = runner.invoke(run, ["list"])
    assert_that(list_result, has_exited_successfully())
    assert_that(
        list_result,
        has_output(matches_regexp("^.*js.+Jane Smith.+jane@example\\.org.*\n$")),
    )


def test_can_edit_added_coauthors(runner) -> None:
    # noinspection PyTypeChecker
    add_result = runner.invoke(
        run,
        ["add", "--id", "js", "--name", "Jane Smith", "--email", "jane@example.org"],
    )
    assert_that(add_result, has_exited_successfully())

    # noinspection PyTypeChecker
    edit_result = runner.invoke(
        run,
        [
            "edit",
            "--id",
            "js",
            "--new-id",
            "as",
            "--name",
            "Amy Smith",
            "--email",
            "amy@example.org",
        ],
    )
    assert_that(edit_result, has_exited_successfully())

    # noinspection PyTypeChecker
    list_result = runner.invoke(run, ["list"])
    assert_that(list_result, has_exited_successfully())
    assert_that(
        list_result,
        has_output(matches_regexp("^.*as.+Amy Smith.+amy@example\\.org.*\n$")),
    )


def test_displays_an_error_when_loading_malformed_coauthor(
    runner, configuration
) -> None:
    # noinspection PyTypeChecker
    result = runner.invoke(run, ["list"])

    assert_that(result, has_exited_exceptionally())
    assert_that(
        result,
        has_error(
            equal_to("Malformed coauthor in configuration: id 'id', value 'value'\n")
        ),
    )
