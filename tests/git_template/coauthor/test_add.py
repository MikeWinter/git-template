# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import Callable

import click.testing
import pytest
from hamcrest import assert_that
from pytest_mock import MockerFixture

from git_template.coauthor import Coauthor, Store
from git_template.coauthor.add import add_coauthors
from ..matcher import has_exited_exceptionally, has_exited_successfully


@pytest.fixture()
def runner() -> click.testing.CliRunner:
    return click.testing.CliRunner(mix_stderr=False)


@pytest.fixture()
def output() -> Callable[[str], None]:
    return click.echo


@pytest.fixture()
def error() -> Callable[[str], None]:
    return lambda m: click.echo(m, err=True)


@pytest.fixture()
def store(mocker: MockerFixture):
    mock = mocker.Mock(Store)
    mock.by_id.return_value = None
    return mock


@pytest.fixture()
def context(store, output, error) -> dict:
    return {
        "store": store,
        "output": output,
        "error": error,
    }


class TestAdd:
    def test_saves_new_coauthor(
        self, store, runner, context, mocker: MockerFixture
    ) -> None:
        from git_template.coauthor.add import EMAIL, ID, NAME

        convert_email_spy = mocker.spy(EMAIL, "convert")
        convert_id_spy = mocker.spy(ID, "convert")
        convert_name_spy = mocker.spy(NAME, "convert")

        # noinspection PyTypeChecker
        result = runner.invoke(
            add_coauthors,
            ["--id", "id", "--name", "name", "--email", "mail@example.org"],
            obj=context,
        )

        assert_that(result, has_exited_successfully())
        # noinspection PyUnresolvedReferences
        store.save.assert_called_once_with(
            Coauthor(id="id", name="name", email="mail@example.org")
        )
        convert_email_spy.assert_called_with("mail@example.org", mocker.ANY, mocker.ANY)
        convert_id_spy.assert_called_once_with("id", mocker.ANY, mocker.ANY)
        convert_name_spy.assert_called_once_with("name", mocker.ANY, mocker.ANY)

    def test_rejects_a_coauthor_with_the_same_id(self, store, runner, context) -> None:
        store.by_id.return_value = Coauthor("id", "name", "mail@example.org")

        # noinspection PyTypeChecker
        result = runner.invoke(
            add_coauthors,
            ["--id", "id", "--name", "name", "--email", "mail@example.org"],
            obj=context,
        )

        assert_that(result, has_exited_exceptionally())
        # noinspection PyUnresolvedReferences
        store.save.assert_not_called()
