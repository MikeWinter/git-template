# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import Callable

import click.testing
import pytest
from hamcrest import assert_that, empty, equal_to, is_, matches_regexp
from pytest_mock import MockerFixture

from git_template.coauthor import Coauthor, Store
from git_template.coauthor.list import TextListView, list_coauthors
from ..matcher import has_error, has_exited_successfully, has_output


@pytest.fixture()
def runner() -> click.testing.CliRunner:
    return click.testing.CliRunner(mix_stderr=False)


@pytest.fixture()
def output() -> Callable[[str], None]:
    return click.echo


@pytest.fixture()
def error() -> Callable[[str], None]:
    return lambda m: click.echo(m, err=True)


@pytest.fixture()
def store(mocker: MockerFixture) -> Store:
    return mocker.Mock(Store)


@pytest.fixture()
def context(store, output, error) -> dict:
    return {
        "store": store,
        "output": output,
        "error": error,
    }


@pytest.fixture()
def no_coauthors(store) -> Store:
    store.all.return_value = []
    return store


@pytest.fixture()
def single_coauthor(store) -> Store:
    store.all.return_value = [Coauthor("jb", "Jane Bloggs", "jane@example.org")]
    return store


class TestTextList:
    def test_returns_no_data_on_standard_output_when_there_are_no_authors(
        self, runner, context, no_coauthors
    ) -> None:
        # noinspection PyTypeChecker
        result = runner.invoke(list_coauthors, obj=context)

        assert_that(result, has_exited_successfully())
        assert_that(result, has_output(empty()))

    def test_informs_the_user_when_no_authors_are_recorded(
        self, runner, context, no_coauthors
    ) -> None:
        # noinspection PyTypeChecker
        result = runner.invoke(list_coauthors, obj=context)

        assert_that(result, has_exited_successfully())
        assert_that(result, has_error("No co-authors have been added.\n"))

    def test_returns_authors_on_standard_output(
        self, runner, context, single_coauthor
    ) -> None:
        # noinspection PyTypeChecker
        result = runner.invoke(list_coauthors, obj=context)

        assert_that(result, has_exited_successfully())
        assert_that(result, has_error(empty()))
        assert_that(
            result,
            has_output(matches_regexp("^jb\\s+Jane Bloggs <jane@example.org>\n$")),
        )


@pytest.fixture
def view(output, error) -> TextListView:
    return TextListView(output, error)


class TestTextListView:
    def test_inform_the_user_if_there_are_no_coauthors(self, view, capsys) -> None:
        view.render([])

        (out, err) = capsys.readouterr()
        assert_that(out, is_(empty()))
        assert_that(err, is_(equal_to("No co-authors have been added.\n")))

    def test_displays_coauthor_details(self, view, capsys) -> None:
        view.render([Coauthor("xx", "the-name", "mail@example.org")])

        (out, err) = capsys.readouterr()
        assert_that(out, matches_regexp("^xx\\s+the-name <mail@example.org>\n$"))
        assert_that(err, is_(empty()))

    def test_aligns_names_despite_different_id_lengths(self, view, capsys) -> None:
        view.render(
            [
                Coauthor("a", "A", "a@example.org"),
                Coauthor("bbb", "B", "b@example.org"),
                Coauthor("cc", "C", "c@example.org"),
            ]
        )

        (out, err) = capsys.readouterr()
        assert_that(
            out,
            # fmt: off
            is_(equal_to(
                "a     A <a@example.org>\n"
                "bbb   B <b@example.org>\n"
                "cc    C <c@example.org>\n"
            )),
            # fmt: on
        )

    def test_does_not_maintain_alignment_if_the_id_is_too_long(
        self, view, capsys
    ) -> None:
        maximum_length_id = "b" * 10
        overly_long_id = "z" * 11

        view.render(
            [
                Coauthor("a", "A", "a@example.org"),
                Coauthor(maximum_length_id, "B", "b@example.org"),
                Coauthor(overly_long_id, "Z", "z@example.org"),
            ]
        )

        (out, err) = capsys.readouterr()
        assert_that(
            out,
            # fmt: off
            is_(equal_to(
                "a            A <a@example.org>\n"
                "bbbbbbbbbb   B <b@example.org>\n"
                "zzzzzzzzzzz   Z <z@example.org>\n"
            )),
            # fmt: on
        )
