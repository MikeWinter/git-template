# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import Callable

import click.testing
import pytest
from hamcrest import assert_that, contains_string
from pytest_mock import MockerFixture

from git_template.coauthor import Coauthor, Store
from git_template.coauthor.edit import edit_coauthors
from ..matcher import has_error, has_exited_exceptionally, has_exited_successfully


@pytest.fixture()
def runner() -> click.testing.CliRunner:
    return click.testing.CliRunner(mix_stderr=False)


@pytest.fixture()
def output() -> Callable[[str], None]:
    return click.echo


@pytest.fixture()
def error() -> Callable[[str], None]:
    return lambda m: click.echo(m, err=True)


@pytest.fixture()
def store(mocker: MockerFixture):
    mock = mocker.Mock(Store)
    mock.by_id.return_value = Coauthor("id", "name", "mail@example.org")
    return mock


@pytest.fixture()
def context(store, output, error) -> dict:
    return {
        "store": store,
        "output": output,
        "error": error,
    }


class TestEdit:
    @pytest.mark.parametrize(
        ["name_args", "expected_name", "email_args", "expected_email"],
        [
            [
                ("--name", "new-name"),
                "new-name",
                ("--email", "new@example.org"),
                "new@example.org",
            ],
            [[], "name", [], "mail@example.org"],
        ],
    )
    def test_updated_existing_coauthor_with_the_same_id(
        self,
        store,
        runner,
        context,
        mocker: MockerFixture,
        name_args,
        email_args,
        expected_name,
        expected_email,
    ) -> None:
        from git_template.coauthor.edit import EMAIL, ID, NAME

        convert_email_spy = mocker.spy(EMAIL, "convert")
        convert_id_spy = mocker.spy(ID, "convert")
        convert_name_spy = mocker.spy(NAME, "convert")

        # noinspection PyTypeChecker
        result = runner.invoke(
            edit_coauthors,
            ["--id", "id", *name_args, *email_args],
            obj=context,
        )

        assert_that(result, has_exited_successfully())
        # noinspection PyUnresolvedReferences
        store.save.assert_called_once_with(
            Coauthor(id="id", name=expected_name, email=expected_email)
        )
        # noinspection PyUnresolvedReferences
        store.delete_by_id.assert_not_called()
        if email_args:
            convert_email_spy.assert_called_with(email_args[1], mocker.ANY, mocker.ANY)
        convert_id_spy.assert_called_with("id", mocker.ANY, mocker.ANY)
        if name_args:
            convert_name_spy.assert_called_with(name_args[1], mocker.ANY, mocker.ANY)

    def test_rejects_an_id_that_does_not_exist(self, store, runner, context) -> None:
        store.by_id.return_value = None

        # noinspection PyTypeChecker
        result = runner.invoke(
            edit_coauthors,
            ["--id", "id", "--name", "name", "--email", "mail@example.org"],
            obj=context,
        )

        assert_that(result, has_exited_exceptionally())
        assert_that(
            result, has_error(contains_string("no co-author with that ID exists"))
        )
        # noinspection PyUnresolvedReferences
        store.save.assert_not_called()

    def test_removes_old_coauthor_if_id_is_changed(
        self, store, runner, context, mocker: MockerFixture
    ) -> None:
        from git_template.coauthor.edit import ID

        convert_spy = mocker.spy(ID, "convert")

        # noinspection PyTypeChecker
        result = runner.invoke(
            edit_coauthors,
            ["--id", "id", "--new-id", "new"],
            obj=context,
        )

        assert_that(result, has_exited_successfully())
        # noinspection PyUnresolvedReferences
        store.save.assert_called_with(
            Coauthor(id="new", name="name", email="mail@example.org")
        )
        # noinspection PyUnresolvedReferences
        store.delete_by_id.assert_called_with("id")
        convert_spy.assert_any_call("new", mocker.ANY, mocker.ANY)
