# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

import pytest
from hamcrest import (
    assert_that,
    calling,
    contains_exactly,
    empty,
    equal_to,
    is_,
    none,
    raises,
)
from pytest_mock import MockerFixture

from git_template.coauthor import Coauthor
from git_template.coauthor.store import MalformedCoauthorException, Store
from git_template.git import Configuration


@pytest.fixture()
def configuration(mocker: MockerFixture):
    configuration = mocker.Mock(Configuration)
    configuration.keys.return_value = []
    return configuration


@pytest.fixture()
def store(configuration) -> Store:
    return Store(configuration)


class TestStore:
    def test_returns_empty_list_if_configuration_is_empty(self, store) -> None:
        coauthors = store.all()

        # noinspection PyTypeChecker
        assert_that(coauthors, is_(empty()))

    def test_returns_saved_coauthors(self, store, configuration) -> None:
        configuration.keys.return_value = ["js"]
        configuration.get.return_value = b"Jane Smith <jane@example.org>"

        coauthors = store.all()

        assert_that(
            coauthors,
            contains_exactly(Coauthor("js", "Jane Smith", "jane@example.org")),
        )

    def test_reports_malformed_coauthors(self, store, configuration) -> None:
        configuration.keys.return_value = "id"
        configuration.get.return_value = b"name"

        # noinspection PyTypeChecker
        assert_that(calling(store.all), raises(MalformedCoauthorException))

    def test_writes_saved_authors_to_configuration(self, store, configuration) -> None:
        store.save(Coauthor("ar", "Alice Rogers", "alice@example.org"))

        # noinspection PyUnresolvedReferences
        configuration.set.assert_called_once_with(
            section="commit",
            subsection="coauthors",
            key="ar",
            value=b"Alice Rogers <alice@example.org>",
        )

    def test_returns_matching_id(self, store, configuration) -> None:
        configuration.get.return_value = b"Alice Rogers <alice@example.org>"

        coauthor = store.by_id("ar")

        assert_that(
            coauthor,
            is_(equal_to(Coauthor("ar", "Alice Rogers", "alice@example.org"))),
        )

    def test_returns_none_if_no_id_matches(self, store, configuration) -> None:
        configuration.get.return_value = None

        coauthor = store.by_id("jd")

        assert_that(coauthor, is_(none()))
