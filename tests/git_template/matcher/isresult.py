# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import AnyStr, Optional, Sized, Union

from click.testing import Result
from hamcrest import equal_to, not_
from hamcrest.core.base_matcher import BaseMatcher
from hamcrest.core.description import Description
from hamcrest.core.matcher import Matcher

__all__ = [
    "has_error",
    "has_exited_successfully",
    "has_exited_exceptionally",
    "has_output",
]


class HasError(BaseMatcher[Result]):
    def __init__(self, error_matcher: Matcher[Union[AnyStr, Sized]]):
        self._error_matcher = error_matcher

    def _matches(self, item: Result) -> bool:
        return self._error_matcher.matches(item.stderr)

    def describe_to(self, description: Description) -> None:
        description.append_text("standard error to match ").append_description_of(
            self._error_matcher
        )

    def describe_mismatch(
        self, item: Result, mismatch_description: Description
    ) -> None:
        mismatch_description.append_text("was ").append_description_of(item.stderr)


class HasExited(BaseMatcher[Result]):
    def __init__(self, status_matcher: Matcher[int]) -> None:
        self._status_matcher = status_matcher

    def _matches(self, item: Result) -> bool:
        return self._status_matcher.matches(item.exit_code)

    def describe_to(self, description: Description) -> None:
        description.append_text(
            "application to exit with status "
        ).append_description_of(self._status_matcher)

    def describe_mismatch(
        self, item: Result, mismatch_description: Description
    ) -> None:
        mismatch_description.append_text("exited with status ").append_description_of(
            item.exit_code
        )


class HasOutput(BaseMatcher[Result]):
    def __init__(self, output_matcher: Matcher[Union[AnyStr, Sized]]) -> None:
        self._output_matcher = output_matcher

    def _matches(self, item: Result) -> bool:
        return self._output_matcher.matches(item.stdout)

    def describe_to(self, description: Description) -> None:
        description.append_text("standard output to match ").append_description_of(
            self._output_matcher
        )

    def describe_mismatch(
        self, item: Result, mismatch_description: Description
    ) -> None:
        mismatch_description.append_text("was ").append_description_of(item.stdout)


def has_error(error: Union[str, Matcher[Union[AnyStr, Sized]]]) -> Matcher[Result]:
    if isinstance(error, str):
        error = equal_to(error)
    return HasError(error)


def has_exited_successfully() -> Matcher[Result]:
    return HasExited(equal_to(0))


def has_exited_exceptionally(
    status: Optional[Union[int, Matcher[int]]] = None
) -> Matcher[Result]:
    if status is None:
        status = not_(equal_to(0))
    elif isinstance(status, int):
        if status == 0:
            raise ValueError(
                "status must not be zero (0); use has_exited_successfully() instead"
            )
        status = equal_to(status)
    return HasExited(status)


def has_output(output: Union[str, Matcher[Union[AnyStr, Sized]]]) -> Matcher[Result]:
    if isinstance(output, str):
        output = equal_to(output)
    return HasOutput(output)
