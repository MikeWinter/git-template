# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from .isresult import (
    has_error,
    has_exited_exceptionally,
    has_exited_successfully,
    has_output,
)

__all__ = [
    "has_error",
    "has_exited_exceptionally",
    "has_exited_successfully",
    "has_output",
]
