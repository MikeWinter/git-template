# git-template

[![GitHub](https://img.shields.io/badge/license-GPL%203.0%20(or%20later)-green)](LICENSES/GPL-3.0-or-later.txt)
[![GitHub](https://img.shields.io/badge/license-CC0%201.0-orange)](LICENSES/CC0-1.0.txt)
[![linting: pylint](https://img.shields.io/badge/linting-pylint-yellowgreen)](https://github.com/PyCQA/pylint)

`git template` and `git coauthor` are tools for managing commit coauthors and
templates. These are similar to tools like [git mob][git-mob], but add more
configurability to template formatting.

[git-mob]: https://www.npmjs.com/package/git-mob

## Installation

Before installing this tool, install [Python][python] 3.7.2 or newer.

[python]: https://www.python.org/downloads/

## Development

A `go` script is included to ease some development tasks. New developers will
find the `init` command useful for installing dependencies and commit hooks,
but the `test` command will probably be the most used.

Use the `help` command to get a list of other possibilities.

### Requirements

* [poetry][poetry] — dependency management
* [pre-commit][pre-commit] — managing commit hooks
* [ruby 3][ruby3] — file linting (required by pre-commit)
* [shellcheck][shellcheck] — linting shell scripts (required by pre-commit)
* [shfmt][shfmt] — formatting shell scripts (required by pre-commit)

[poetry]: https://python-poetry.org/docs/#installation
[pre-commit]: https://pre-commit.com/#install
[ruby3]: https://www.ruby-lang.org/en/documentation/installation/
[shellcheck]: https://github.com/koalaman/shellcheck#installing
[shfmt]: https://github.com/mvdan/sh#shfmt

## License

This tool is licensed under [GPL v3.0 or later][gpl3+]. The copyright of
various supplementary and generated files is waived under the [Creative Commons
Public Domain Dedication (CC0)][cc0].

SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
SPDX-License-Identifier: CC0-1.0

[cc0]: LICENSES/CC0-1.0.txt
[gpl3+]: LICENSES/GPL-3.0-or-later.txt
