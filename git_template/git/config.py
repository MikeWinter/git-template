"""Facilitates access to Git configuration."""

# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

import re
import subprocess
from pathlib import Path
from typing import Optional, Sequence


class Configuration:
    """Read from, and write to, Git configuration."""

    __slots__ = ("_path",)

    def __init__(self, path: Optional[Path] = None):
        self._path = path if path else Path.cwd()

    def keys(self, section: str, subsection: Optional[str] = None) -> Sequence[str]:
        """Return the keys in a given configuration (sub-)section."""
        qualifier = self._build_qualifier(section, subsection)
        try:
            result = self._git_config(
                "--null",
                "--name-only",
                "--get-regexp",
                rf"^{re.escape(qualifier)}\.[^.]+$",
            )
            fully_qualified_names = [
                name.decode() for name in result.stdout.split(sep=bytes(1))[:-1]
            ]
            return [name.rpartition(".")[2] for name in fully_qualified_names]
        except subprocess.CalledProcessError as err:
            if err.returncode == 1:
                return []
            raise

    def get(
        self,
        section: str,
        key: str,
        subsection: Optional[str] = None,
        default: Optional[bytes] = None,
    ) -> Optional[bytes]:
        """Return the value of the specified configuration key."""
        _, _ = subsection, default
        try:
            result = self._git_config(
                "--null",
                "--get",
                f"{self._build_qualifier(section, subsection)}.{key}",
            )
            return result.stdout[:-1]
        except subprocess.CalledProcessError as err:
            if err.returncode == 1:
                return default
            raise

    def set(
        self, section: str, key: str, value: bytes, subsection: Optional[str] = None
    ) -> None:
        """Modify the value of the specified configuration key."""
        self._git_config(f"{self._build_qualifier(section, subsection)}.{key}", value)

    def unset(self, section: str, key: str, subsection: Optional[str] = None) -> None:
        """Remove the specified configuration key."""
        self._git_config(
            "--unset", f"{self._build_qualifier(section, subsection)}.{key}"
        )

    def _git_config(self, *args: Sequence[str]) -> subprocess.CompletedProcess:
        return subprocess.run(
            [
                "git",
                "config",
                *args,
            ],
            cwd=self._path,
            stdout=subprocess.PIPE,
            stderr=subprocess.DEVNULL,
            check=True,
        )

    @staticmethod
    def _build_qualifier(section: str, subsection: Optional[str]) -> str:
        return f"{section}.{subsection}" if subsection else section
