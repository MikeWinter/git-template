"""Lists commit co-authors."""

# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from functools import reduce
from typing import Callable, Sequence

import click

from .store import MalformedCoauthorException, Store
from .types import Coauthor


class TextListView:  # pylint: disable=too-few-public-methods
    """Display the list of co-authors as text."""

    _MAX_ID_WIDTH = 10

    __slots__ = ("_output", "_error")

    def __init__(
        self, output: Callable[[str], None], error: Callable[[str], None]
    ) -> None:
        self._output = output
        self._error = error

    def render(self, coauthors: Sequence[Coauthor]) -> None:
        """Write the commit co-authors details to the streams."""
        if coauthors:
            max_id_length = self.__find_max_id_length(coauthors)

            for coauthor in coauthors:
                padded_id = self.__pad_id(coauthor.id, max_id_length)
                self._output(f"{padded_id}   {coauthor.name} <{coauthor.email}>")

        else:
            self._error("No co-authors have been added.")

    @classmethod
    def __find_max_id_length(cls, coauthors: Sequence[Coauthor]) -> int:
        max_length = reduce(
            lambda length, item: max(length, len(item.id)), coauthors, 0
        )
        return min(max_length, cls._MAX_ID_WIDTH)

    # noinspection PyShadowingBuiltins
    @staticmethod
    def __pad_id(
        id: str, max_id_length: int  # pylint: disable=invalid-name,redefined-builtin
    ) -> str:
        padding = " " * (max_id_length - len(id))
        return id + padding


@click.command(name="list")
@click.pass_context
def list_coauthors(ctx: click.Context) -> None:
    """List previously added commit co-authors."""
    store: Store = ctx.obj["store"]
    output = ctx.obj["output"]
    error = ctx.obj["error"]
    view = TextListView(output, error)

    try:
        view.render(store.all())
    except MalformedCoauthorException as exc:
        error(exc.message)
        ctx.exit(3)
