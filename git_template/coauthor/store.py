"""Manages commit co-author storage."""

# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

import re
from typing import Optional, Sequence

from .types import Coauthor
from ..git import Configuration


class Store:
    """Facilitates access to registered commit co-authors."""

    _ENTRY_PATTERN = re.compile(r"(?P<name>(?:[^<]|\\<)+)\s+<(?P<email>(?:[^>]|\\>)+)>")
    _SECTION = "commit"
    _SUBSECTION = "coauthors"

    __slots__ = ("_configuration",)

    def __init__(self, configuration: Configuration) -> None:
        self._configuration = configuration

    def all(self) -> Sequence[Coauthor]:
        """Return all registered co-authors."""
        coauthors = []

        for id_ in self._configuration.keys(
            section=self._SECTION, subsection=self._SUBSECTION
        ):
            entry = self._configuration.get(
                section=self._SECTION, subsection=self._SUBSECTION, key=id_
            )
            coauthor = self._to_coauthor(id_, entry.decode())
            coauthors.append(coauthor)

        return coauthors

    # noinspection PyShadowingBuiltins
    def by_id(
        self, id: str  # pylint: disable=invalid-name,redefined-builtin
    ) -> Optional[Coauthor]:
        """Return the co-author with the same ID; None, otherwise."""
        entry = self._configuration.get(
            section=self._SECTION, subsection=self._SUBSECTION, key=id
        )
        return self._to_coauthor(id, entry.decode()) if entry else None

    def save(self, coauthor: Coauthor) -> None:
        """
        Store the given co-author.

        Updates an existing co-author with the same ID.
        """
        entry = self._build_entry(coauthor)
        self._configuration.set(
            key=coauthor.id,
            value=entry.encode(),
            section=self._SECTION,
            subsection=self._SUBSECTION,
        )

    # noinspection PyShadowingBuiltins
    def delete_by_id(
        self, id: str  # pylint: disable=invalid-name,redefined-builtin
    ) -> None:
        """
        Remove the co-author that has the given ID.
        """
        self._configuration.unset(
            key=id,
            section=self._SECTION,
            subsection=self._SUBSECTION,
        )

    # noinspection PyMethodMayBeStatic
    def _build_entry(self, coauthor: Coauthor) -> str:
        return f"{coauthor.name} <{coauthor.email}>"

    # noinspection PyShadowingBuiltins
    def _to_coauthor(
        self, id: str, entry: str  # pylint: disable=invalid-name,redefined-builtin
    ) -> Coauthor:
        match = self._ENTRY_PATTERN.match(entry)
        if not match:
            raise MalformedCoauthorException(id, entry)
        name, email = match.group("name"), match.group("email")
        return Coauthor(id, name, email)


class MalformedCoauthorException(Exception):
    """Malformed coauthor definition."""

    __slots__ = ("_id", "_value")

    # noinspection PyShadowingBuiltins
    def __init__(self, id, value, *args):  # pylint: disable=redefined-builtin
        super().__init__(args)
        self._id = id
        self._value = value

    @property
    def message(self) -> str:
        """A formatted message describing this exception."""
        return (
            "Malformed coauthor in configuration: "
            f"id '{self._id}', value '{self._value}'"
        )
