"""Manages the registration of Git commit co-authors."""

# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from .store import Store
from .types import Coauthor

__all__ = ["Coauthor", "Store"]
