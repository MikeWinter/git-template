"""Edits commit co-authors."""

# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

import click

from .store import Store
from .types import Coauthor
from ..params import EMAIL, ID, NAME

_ID_HELP_TEXT = "The ID of the co-author to update."
_NEW_ID_HELP_TEXT = "The updated ID of the co-author."
_NAME_HELP_TEXT = "The updated name of the co-author."
_EMAIL_HELP_TEXT = "The updated email address of the co-author."


# noinspection PyShadowingBuiltins
@click.command(name="edit")
@click.option("--id", required=True, type=ID, help=_ID_HELP_TEXT)
@click.option("--new-id", type=ID, help=_NEW_ID_HELP_TEXT)
@click.option("--name", type=NAME, help=_NAME_HELP_TEXT)
@click.option("--email", type=EMAIL, help=_EMAIL_HELP_TEXT)
@click.pass_context
def edit_coauthors(
    ctx: click.Context,
    id: str,  # pylint: disable=invalid-name,redefined-builtin
    new_id: str,
    name: str,
    email: str,
) -> None:
    """Edit the details of a commit co-author."""
    store: Store = ctx.obj["store"]

    existing_coauthor = store.by_id(id)
    if existing_coauthor is None:
        raise click.BadParameter("no co-author with that ID exists", param_hint="--id")

    coauthor = Coauthor(
        new_id if new_id is not None else id,
        name if name is not None else existing_coauthor.name,
        email if email is not None else existing_coauthor.email,
    )
    store.save(coauthor)
    if new_id != id and new_id is not None:
        store.delete_by_id(id)
