"""The command-line interface for managing Git commit co-authors."""

# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

import click

from .add import add_coauthors
from .edit import edit_coauthors
from .list import list_coauthors
from .store import Store
from ..git import Configuration


@click.group()
@click.pass_context
def run(ctx: click.Context) -> None:
    """List, add, or delete commit co-authors."""
    ctx.ensure_object(dict)
    git_configuration = Configuration()

    ctx.obj["store"] = Store(git_configuration)
    ctx.obj["output"] = output
    ctx.obj["error"] = error


def output(message: str) -> None:
    """Write a message to standard output."""
    click.echo(message)


def error(message: str) -> None:
    """Write a message to standard error."""
    click.echo(message, err=True)


for command in [add_coauthors, list_coauthors, edit_coauthors]:
    # noinspection PyTypeChecker
    run.add_command(command)
