"""Adds commit co-authors."""

# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

import click

from .store import Store
from .types import Coauthor
from ..params import EMAIL, ID, NAME


_ID_HELP_TEXT = (
    "The ID to associate with this co-author. This value will be used by other "
    "commands to reference the co-author.\n\nAn ID may only consist of letters "
    "and numbers."
)
_NAME_HELP_TEXT = "The name of the co-author."
_EMAIL_HELP_TEXT = "The email address of the co-author."


# noinspection PyShadowingBuiltins
@click.command(name="add")
@click.option("--id", required=True, type=ID, help=_ID_HELP_TEXT)
@click.option("--name", required=True, type=NAME, help=_NAME_HELP_TEXT)
@click.option("--email", required=True, type=EMAIL, help=_EMAIL_HELP_TEXT)
@click.pass_context
def add_coauthors(
    ctx: click.Context,
    id: str,  # pylint: disable=invalid-name,redefined-builtin
    name: str,
    email: str,
) -> None:
    """Add a commit co-author."""
    store: Store = ctx.obj["store"]

    if store.by_id(id) is not None:
        raise click.BadParameter(
            "a co-author with that ID already exists", param_hint="--id"
        )

    coauthor = Coauthor(id, name, email)
    store.save(coauthor)
