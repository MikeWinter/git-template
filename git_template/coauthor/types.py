"""Coauthor domain types."""

# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import NamedTuple


class Coauthor(NamedTuple):
    """Describes a co-author."""

    id: str
    name: str
    email: str
