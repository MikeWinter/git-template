"""Name parameter validation."""

# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

import re
from typing import Any, Optional

from click import Context, ParamType, Parameter


class NameParamType(ParamType):
    """Represents name parameters."""

    _NAME_FORMAT = re.compile(r"^[^<@>]+$")

    name = "name"

    def convert(
        self, value: Any, param: Optional[Parameter], ctx: Optional[Context]
    ) -> Any:
        if not self._NAME_FORMAT.search(value):
            self.fail(
                f"{value!r} must not contain an email address"
                " (no '<', '>', or '@' symbols)",
                param,
                ctx,
            )
        return value


NAME = NameParamType()
