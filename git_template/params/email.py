"""Email parameter validation."""

# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import Any, Optional

from click import Context, ParamType, Parameter
from validators import email


class EmailParamType(ParamType):
    """Represents email address parameters."""

    name = "email address"

    def convert(
        self, value: Any, param: Optional[Parameter], ctx: Optional[Context]
    ) -> Any:
        if not email(value):
            self.fail(
                f"{value!r} is not an email address (e.g. mail@example.org)", param, ctx
            )
        return value


EMAIL = EmailParamType()
