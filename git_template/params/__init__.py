"""Represents parameter types."""

# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from .email import EMAIL, EmailParamType
from .id import ID, IdParamType
from .name import NAME, NameParamType

__all__ = ["EMAIL", "EmailParamType", "ID", "IdParamType", "NAME", "NameParamType"]
