"""ID parameter validation."""

# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

import re
from typing import Any, Optional

from click import Context, ParamType, Parameter


class IdParamType(ParamType):
    """Represents ID parameters."""

    _ID_FORMAT = re.compile(r"^[a-z\d]+$", re.IGNORECASE)

    name = "ID"

    def convert(
        self, value: Any, param: Optional[Parameter], ctx: Optional[Context]
    ) -> Any:
        if not self._ID_FORMAT.search(value):
            self.fail(
                f"{value!r} is not an ID (i.e. only letters and numbers)", param, ctx
            )
        return value


ID = IdParamType()
