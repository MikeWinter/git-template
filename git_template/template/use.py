"""Applies a commit template layout."""

# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

import click

from .context import ContextValues
from .layout import Layout
from .params import LAYOUT


@click.command(name="use")
@click.argument("layout", type=LAYOUT)
@click.pass_obj
def use_layout(obj: ContextValues, layout: Layout) -> None:
    """Select a layout to use for creating a commit template."""

    repository = obj.repo

    template_path = repository.common_path.joinpath("templates", layout.name)
    template_path.parent.mkdir(parents=True, exist_ok=True)
    with template_path.open("w", encoding="utf-8") as template:
        layout.render(template)

    repository.configuration.set(
        "commit",
        "template",
        bytes(template_path.relative_to(repository.work_tree_path)),
    )
    repository.configuration.set(
        "commit.template", "layout", layout.name.encode("utf-8")
    )
