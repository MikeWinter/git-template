"""Provides access to the Git repository."""

# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from functools import partial
from pathlib import Path
from typing import Callable, Iterable, Tuple, Union

from dulwich.errors import NotGitRepository
from dulwich.repo import Repo

PathLike = Union[str, Path]
SectionLike = Union[str, Iterable[str]]

_to_bytes: Callable[[str], bytes] = partial(str.encode, encoding="utf-8")


class Configuration:
    """The configuration of the Git repository."""

    __slots__ = ("_repo",)

    def __init__(self, repo: Repo) -> None:
        self._repo = repo

    def get(self, section: SectionLike, key: str) -> bytes:
        """Retrieves the contents of a configuration value."""
        section = self._section_as_tuple(section)

        config = self._repo.get_config()
        return config.get(section, key)

    def set(self, section: SectionLike, key: str, value: bytes) -> None:
        """Sets a configuration value."""
        section = self._section_as_tuple(section)
        key = key.encode("utf-8")

        config = self._repo.get_config()
        if section in config and key in config[section]:
            del config[section][key]
        config.set(section, key, value)
        config.write_to_path()

    def remove(self, section: SectionLike, key: str) -> None:
        """Removes a configuration value."""
        section = self._section_as_tuple(section)
        key = key.encode("utf-8")

        config = self._repo.get_config()
        if section in config and key in config[section]:
            del config[section][key]
            config.write_to_path()

    @staticmethod
    def _section_as_tuple(section: SectionLike) -> Tuple[bytes]:
        if isinstance(section, str):
            section = tuple(section.split(".", maxsplit=1))
        elif isinstance(section, Iterable):
            section_name, *subsection_names = tuple(section)
            section = (
                (section_name, ".".join(subsection_names))
                if subsection_names
                else (section_name,)
            )
        return tuple(map(_to_bytes, section))


class Repository:
    """The Git repository."""

    __slots__ = ("_repo",)

    def __init__(self, path: PathLike) -> None:
        try:
            self._repo = Repo.discover(str(path))
        except NotGitRepository as exc:
            raise NoGitRepositoryFoundError(path) from exc

    @classmethod
    def discover(cls, path: PathLike = ".") -> "Repository":
        """Finds the root of a Git repository at the given path or its ancestors."""
        return cls(path)

    @property
    def git_path(self) -> Path:
        """The path of the Git directory in this repository."""
        return Path(self._repo.controldir())

    @property
    def common_path(self) -> Path:
        """The path of the common directory in this repository."""
        return Path(self._repo.commondir())

    @property
    def work_tree_path(self) -> Path:
        """The path of the working tree in this repository."""
        return Path(self._repo.path)

    @property
    def configuration(self) -> Configuration:
        """The configuration of this repository."""
        return Configuration(self._repo)


class NoGitRepositoryFoundError(Exception):
    """Unable to find a Git repository."""

    __slots__ = ("path",)

    def __init__(self, path: PathLike) -> None:
        super().__init__(f"No Git repository found at '{path}'")
        self.path = path
