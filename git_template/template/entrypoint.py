"""The command-line interface for managing Git commit templates."""

# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from pathlib import Path

import click
from xdg import xdg_data_dirs, xdg_data_home

from .context import ContextValues
from .layout import LayoutStore
from .repository import Repository
from .use import use_layout


def _to_layouts_path(path: Path) -> Path:
    return path.joinpath("git-template", "layouts")


@click.group()
@click.pass_context
def run(ctx: click.Context):
    """Generate and edit commit templates."""

    search_paths = [xdg_data_home(), *xdg_data_dirs()]
    layout_search_paths = map(_to_layouts_path, search_paths)
    ctx.obj = ContextValues(
        layouts=LayoutStore(layout_search_paths),
        repo=Repository.discover(),
    )


for command in [use_layout]:
    # noinspection PyTypeChecker
    run.add_command(command)
