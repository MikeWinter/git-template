"""Layout parameter validation."""

# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import Any, Optional

from click import Context, ParamType, Parameter

from ..context import ContextValues
from ..layout import LayoutNotFoundError


class LayoutParamType(ParamType):
    """Represents layout parameters."""

    name = "layout"

    # pylint: disable=inconsistent-return-statements
    def convert(
        self, value: Any, param: Optional[Parameter], ctx: Optional[Context]
    ) -> Any:
        obj = ctx.find_object(ContextValues)
        try:
            return obj.layouts.by_name(value)
        except LayoutNotFoundError:
            self.fail(f"unable to find a layout named '{value}'", param, ctx)


LAYOUT = LayoutParamType()
