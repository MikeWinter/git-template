"""Represents template-specific parameter types."""

# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from .layout import LAYOUT, LayoutParamType

__all__ = ["LAYOUT", "LayoutParamType"]
