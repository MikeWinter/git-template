"""Loading and rendering commit template layouts."""

# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from pathlib import Path
from typing import Iterable, TextIO

from liquid import Environment
from liquid.exceptions import TemplateNotFound
from liquid.loaders import FileExtensionLoader
from liquid.template import BoundTemplate


class Layout:
    """A commit template layout."""

    __slots__ = ("_template",)

    def __init__(self, template: BoundTemplate) -> None:
        self._template = template

    def render(self, file: TextIO) -> None:
        """
        Renders this layout into the specified file object.

        The file object must be writable. The template will be rendered at the current
        position within the file.
        """
        file.write(self._template.render())

    @property
    def name(self) -> str:
        """The name of the template."""
        # noinspection PyTypeChecker
        return self._template.name

    @property
    def path(self) -> Path:
        """The path to the layout definition."""
        # noinspection PyTypeChecker
        return Path(self._template.path)


# pylint: disable=too-few-public-methods
class LayoutStore:
    """Finds and loads commit template layouts."""

    __slots__ = ("_environment",)

    def __init__(self, search_paths: Iterable[Path]) -> None:
        loader = FileExtensionLoader(search_paths)
        self._environment = Environment(loader=loader)

    def by_name(self, name: str) -> Layout:
        """Finds the named layout."""
        try:
            return Layout(self._environment.get_template(name))
        except TemplateNotFound as exc:
            raise LayoutNotFoundError from exc


class LayoutNotFoundError(Exception):
    """Unable to find the specified layout definition."""
