"""Template types."""

# SPDX-FileCopyrightText: 2020–2023 Mike Winter <mail@mikewinter.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import NamedTuple

from .layout import LayoutStore
from .repository import Repository


class ContextValues(NamedTuple):
    """The structure of context objects."""

    layouts: LayoutStore
    repo: Repository
